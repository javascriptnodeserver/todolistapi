var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Task = require('./api/models/todoListModel'), //created model loading here
  bodyParser = require('body-parser');
  
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Tododb'); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/todoListRoutes'); //importing route
routes(app); //register the route


app.listen(port);


console.log('todo lista RESTful API server started on: ' + port);

console.log('todo lista RESTful API server started going on: ' + port);

//dicas do clone https://danielkummer.github.io/git-flow-cheatsheet/
// var express = require('express'),
//   app = express(),
//   port = process.env.PORT || 3000;               //ctrl+k+c comenta várias linhas

// app.listen(port);                                //crtl+k+u descomenta

// console.log('todo list RESTful API server started on: ' + port);
